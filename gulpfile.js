var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var purify = require('gulp-purifycss');
var nunjucksRender = require('gulp-nunjucks-render');
var data = require('gulp-data');
var htmlmin = require('gulp-htmlmin');
var browserSync = require('browser-sync').create();

// Static server
gulp.task('online', ['compile'], function () {
    browserSync.init({
        server: {
            baseDir: "./app"
        }
    });
    gulp.watch("templates/**/*.html", ['compile']);
    gulp.watch("templates/**/*.nunjucks", ['compile']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});
gulp.task('deploy', ['minify-css', 'minify-html', 'copy']);

gulp.task('minify-css', function () {
    return gulp.src('app/*.css')
        .pipe(cleanCSS({
            debug: true
        }, function (details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(gulp.dest('build'));
});

gulp.task('minify-html', function () {
    return gulp.src('app/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('build'));
});

gulp.task('compile', function () {
    // Gets .html and .nunjucks files in pages
    return gulp.src('templates/pages/**/*.+(html|nunjucks)')
        // Renders template with nunjucks
        .pipe(data(function () {
            return require('./templates/data.json')
        }))
        .pipe(nunjucksRender({
            path: ['templates/components']
        }))
        // output files in app folder
        .pipe(gulp.dest('app'))
});

gulp.task('copy', function () {
    //gets assets
    gulp.src(['app/css/*'])
        .pipe(gulp.dest('build/css'))
    gulp.src(['app/fonts/*'])
        .pipe(gulp.dest('build/fonts'))
    gulp.src('app/js/*')
        .pipe(gulp.dest('build/js'))
        // Gets .html and .nunjucks files in pages
});
